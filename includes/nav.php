<!-- header begin -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="/">
                                <img class="logo" src="images/vossen-logo-2.png" alt="">
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->
                        
						<!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a href="/">Home</a></li>
                                <li><a href="/shop">Shop Art</a>
                                    <ul>
                                        <li><a href="/shop/modern">Modern</a></li>
                                        <li><a href="/shop/asian">Asian</a></li>
                                        <li><a href="/shop/abstract">Abstract</a></li>
										<li><a href="/shop/wildlife">Wildlife</a></li>
                                        <li><a href="/shop/minimalism">Minimalism</a></li>
                                        <li><a href="/shop/city">City</a></li>
                                    </ul>
                                </li>
                                <li><a href="/blog">Blog</a></li>
                                <li><a href="/about">About Us</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
<!-- header close -->