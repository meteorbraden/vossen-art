        <!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <img src="images/vossen-logo.png" class="logo-small" alt=""><br>
							Vossen has a passion for modern interior design and our mission is to provide the best quality artwork at the best price.                    
						</div>
                    <div class="col-md-4">
                        <div class="widget widget_recent_post">
                            <h3>Art Styles</h3>
                            <ul>
                                <li><a href="/shop/modern">Modern</a></li>
                                <li><a href="/shop/asian">Asian</a></li>
                                <li><a href="/shop/abstract">Abstract</a></li>
								<li><a href="/shop/wildlife">Wildlife</a></li>
                                <li><a href="/shop/minimalism">Minimalism</a></li>
                                <li><a href="/shop/city">City</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
	                    <div class="widget widget_recent_post">
						<h3>Sitemap</h3>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/shop">Shop</a></li>
                                <li><a href="/about">About Us</a></li>
                                <li><a href="/contact">Contact</a></li>                            
                                <li><a href="/privacy-policy">Privacy Policy</a></li>
                                <li><a href="/return-policy">Return Policy</a></li>
                                <li><a href="/shipping-and-delivery">Shipping and Delivery</a></li>                            
                            </ul>
		                </div>
		            </div>
                </div>
            </div>
            
            <div class="subfooter">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            &copy; Copyright 2016 - Vossen
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="social-icons">
                                <a href="https://www.facebook.com/Vossen-1805219913085693/" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
								<a href="https://www.instagram.com/vossenart/?hl=en" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        <!-- footer close -->