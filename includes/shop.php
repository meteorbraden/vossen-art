<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vossen - Shop <?php echo $category ?> Modern Interior Artwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive Minimal Bootstrap Theme">
    <meta name="keywords" content="onepage,responsive,minimal,bootstrap,theme">
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="../css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="../css/animate.css" type="text/css">
    <link rel="stylesheet" href="../css/plugin.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
    <link rel="stylesheet" href="../css/product-flexslider.css" type="text/css">
	<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css" />
    
    <!-- custom background -->
    <link rel="stylesheet" href="../css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="../css/color.css" type="text/css" id="colors">

    <!-- revolution slider -->
    <link rel="stylesheet" href="../rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="../css/rev-settings.css" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="../fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="../onts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="../fonts/elegant_font/HTML_CSS/lte-ie7.js" type="text/css">
    <script src="../js/shopify-cart.js"></script>
    
</head>
<body class="page-shop">

    <div id="wrapper">
       <?php include '../includes/nav2.php'; ?>

       
	<?php 
	
	$url = $_SERVER['REQUEST_URI'];

        	
	if (strpos($url, "?product=")!==false){
		$product_id = explode('=', $url);
		$id = $product_id[1];

			$response = json_decode(file_get_contents($baseUrl.'products.json'));
			foreach ($response->products as $product) {
				
				if ($product->handle == $id){
					$result = json_decode(json_encode($product),true);
					
				}
			}
		
	?>

	 <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Shop</h1>
                        <ul class="crumb">
                            <li><a href="/">Home</a></li>
                            <li class="sep">/</li>
                            <li><a href="/shop">Shop</a></li>
	                        <li class="sep">/</li>
	                        <li><a href="/shop/<?php echo $category ?>"><?php echo $category ?></a></li>
	                        <li class="sep">/</li>
                            <li><?php echo $result['title']; ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->
        
		<div id="content">
			<div class="container">
			    <div class="col-md-5">
					<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
						<div class="flexslider">
							<div class="slider-wrap" data-lightbox="gallery">												
								<?php 
									foreach ($result['images'] as $v) {
										$image = $v['src'];
								?>	
									<div class="slide" data-thumb="<?php echo $image ?>"><a href="<?php echo $image ?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo $image?>" alt="Pink Printed Dress"></a></div>
						
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			        <div class="col-md-7">
			            <div class="project-info">
			                <h2><?php echo $result['title'];?></h2>
			                
			                <div id='<?php echo $result['handle']?>'></div>
							<script type="text/javascript">!function(){function b(){var b=document.createElement("script");b.async=!0,b.src=a,(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(b),b.onload=c}function c(){var a=ShopifyBuy.buildClient({domain:"vossen-art.myshopify.com",apiKey:"744ac21e24a9eddb3cbcb12afc3fdbf9",appId:"6"});ShopifyBuy.UI.onReady(a).then(function(a){a.createComponent("product",{id:[<?php echo $result['id']?>],node:document.getElementById("<?php echo $result['handle']?>"),moneyFormat:"%24%7B%7Bamount%7D%7D",options:{product:{variantId:"all",width:"240px",contents:{img:!1,title:!1,variantTitle:!1,price:!1,description:!1,buttonWithQuantity:!1,quantity:!1},styles:{product:{"text-align":"left","@media (min-width: 601px)":{"max-width":"calc(25% - 20px)","margin-left":"20px","margin-bottom":"50px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},cart:{contents:{button:!0},styles:{button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},footer:{"background-color":"#ffffff"}}},modalProduct:{contents:{variantTitle:!1,buttonWithQuantity:!0,button:!1,quantity:!1},styles:{product:{"@media (min-width: 601px)":{"max-width":"100%","margin-left":"0px","margin-bottom":"0px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},toggle:{styles:{toggle:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},count:{color:"#ffffff",":hover":{color:"#ffffff"}},iconPath:{fill:"#ffffff"}}},productSet:{styles:{products:{"@media (min-width: 601px)":{"margin-left":"-20px"}}}}}})})}var a="https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js";window.ShopifyBuy&&window.ShopifyBuy.UI?c():b()}();</script>
							
							<br />
							
							<h4>Details</h4>
			                <p><?php echo $result['body_html']?></p>
			            </div>
				    </div>
				</div>
			</div>
	        
        

<?php }
	
	else { ?>
	
	 <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo $category ?> Art</h1>
                        <ul class="crumb">
                            <li><a href="/">Home</a></li>
                            <li class="sep">/</li>
                            <li><a href="/shop">Shop</a></li>
                            <li class="sep">/</li>
                            <li><?php echo $category ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->
        
        <!-- content begin -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="products">
	                            
	                            <?php
			
									// Get products 
								    // --------------------------------
							          
										   $response = json_decode(file_get_contents($baseUrl.'products.json')); 
											   foreach ($response->products as $product) {
												   if ($product->product_type == $category){
														$result = json_decode(json_encode($product),true);
										
								?>
	                                <li class="col-md-4 product">
	                                    <a href="?product=<?php echo $result['handle']; ?>"><img src="<?php echo $result['image']['src'];?>" class="img-responsive" alt=""></a>
	                                    <h4 class="product-title"><?php echo $result['title']; ?></h4>
	                                    <div class="price">$<?php echo $result['variants'][0]['price']; ?></div>
	                                    <a href="?product=<?php echo $result['handle']; ?>" class="btn btn-line">View</a>
	                                </li>
                                
                                <?php 
									}}
								 ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php include '../includes/footer2.php'; ?>
    </div>
    



    <!-- Javascript Files
    ================================================== -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jpreLoader.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/easing.js"></script>
    <script type="text/javascript" src="../js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="../js/plugins2.js"></script>
    <script src="../js/jquery.scrollto.js"></script>
    <script src="../js/owl.carousel.js"></script>
    <script src="../js/jquery.countTo.js"></script>
    <script src="../js/classie.js"></script>
    <script src="../js/video.resize.js"></script>
    <script src="../js/validation.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/jquery.magnific-popup.min.js"></script>
    <script src="../js/enquire.min.js"></script>
    <script src="../js/designesia.js"></script>
    <script type="text/javascript" src="../js/functions.js"></script>


    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
</body>
</html>
