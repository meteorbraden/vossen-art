<?php 
		
	$baseUrl = 'https://a3100f4a8cfdd43a25697bf7044fe910:c0149ee5cb7b9bae194eddb315cfff8c@vossen-art.myshopify.com/admin/';
	set_time_limit(0); 

	$headers = get_headers($baseUrl.'shop.json', 1); //this will fetch the headers from Shopify and the second parameter specifies the function to parse it into an array structure
	$apiLimit = (int) str_replace('/500', '', $headers['HTTP_X_SHOPIFY_SHOP_API_CALL_LIMIT']); 
	$response = json_decode(file_get_contents($baseUrl.'products/count.json')); //request it from the shopify api
	$productCount = $response->count; //get the count parameter
	$pages = ceil($productCount/250); //get the amount of pages with max 250 products per page
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>		

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vossen - Shop Modern Interior Artwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Shop modern, stylish, and affordable art.">
    <meta name="keywords" content="modern art, budget art, abstract art, cheap art, canvas art, cool art">
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


        <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="../css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="../css/animate.css" type="text/css">
    <link rel="stylesheet" href="../css/plugin.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
    <link rel="stylesheet" href="../css/product-flexslider.css" type="text/css">
	<link rel="stylesheet" href="../css/magnific-popup.css" type="text/css" />
    
    <!-- custom background -->
    <link rel="stylesheet" href="../css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="../css/color.css" type="text/css" id="colors">

    <!-- revolution slider -->
    <link rel="stylesheet" href="../rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="../css/rev-settings.css" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="../fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="../onts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="../fonts/elegant_font/HTML_CSS/lte-ie7.js" type="text/css">
    <?php include_once("includes/analyticstracking.php") ?>
    <script src="../js/shopify-cart.js"></script>
    
</head>
<body class="page-shop">

    <div id="wrapper">
       <?php include 'includes/nav2.php'; ?>

       
	<?php 
	
	$url = $_SERVER['REQUEST_URI'];
        	
	if (strpos($url, "?product=")!==false){
		$product_id = explode('=', $url);
		$id = $product_id[1];

			$response = json_decode(file_get_contents($baseUrl.'products.json'));
			foreach ($response->products as $product) {
				
				if ($product->handle == $id){
					$result = json_decode(json_encode($product),true);
					
				}
			}
		
	?>

	 <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Shop</h1>
                        <ul class="crumb">
                            <li><a href="/">Home</a></li>
                            <li class="sep">/</li>
                            <li><a href="/shop">Shop</a></li>
	                        <li class="sep">/</li>
                            <li><?php echo $result['title']; ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->
        
		<div id="content">
			<div class="container">
			    <div class="col-md-5">
					<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
						<div class="flexslider">
							<div class="slider-wrap" data-lightbox="gallery">												
								<?php 
									foreach ($result['images'] as $v) {
										$image = $v['src'];
								?>	
									<div class="slide" data-thumb="<?php echo $image ?>"><a href="<?php echo $image ?>" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?php echo $image?>" alt="Pink Printed Dress"></a></div>
						
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			        <div class="col-md-7">
			            <div class="project-info">
			                <h2><?php echo $result['title'];?></h2>
			                
			                <div id='<?php echo $result['handle']?>'></div>
							<script type="text/javascript">!function(){function b(){var b=document.createElement("script");b.async=!0,b.src=a,(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(b),b.onload=c}function c(){var a=ShopifyBuy.buildClient({domain:"vossen-art.myshopify.com",apiKey:"744ac21e24a9eddb3cbcb12afc3fdbf9",appId:"6"});ShopifyBuy.UI.onReady(a).then(function(a){a.createComponent("product",{id:[<?php echo $result['id']?>],node:document.getElementById("<?php echo $result['handle']?>"),moneyFormat:"%24%7B%7Bamount%7D%7D",options:{product:{variantId:"all",width:"240px",contents:{img:!1,title:!1,variantTitle:!1,price:!1,description:!1,buttonWithQuantity:!1,quantity:!1},styles:{product:{"text-align":"left","@media (min-width: 601px)":{"max-width":"calc(25% - 20px)","margin-left":"20px","margin-bottom":"50px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},cart:{contents:{button:!0},styles:{button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},footer:{"background-color":"#ffffff"}}},modalProduct:{contents:{variantTitle:!1,buttonWithQuantity:!0,button:!1,quantity:!1},styles:{product:{"@media (min-width: 601px)":{"max-width":"100%","margin-left":"0px","margin-bottom":"0px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},toggle:{styles:{toggle:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},count:{color:"#ffffff",":hover":{color:"#ffffff"}},iconPath:{fill:"#ffffff"}}},productSet:{styles:{products:{"@media (min-width: 601px)":{"margin-left":"-20px"}}}}}})})}var a="https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js";window.ShopifyBuy&&window.ShopifyBuy.UI?c():b()}();</script>
							
							<br />
							
							<h4>Details</h4>
			                <p><?php echo $result['body_html']?></p>
			            </div>
				    </div>
					    <div class="col-md-12">
						    
					    </div>
				</div>
			</div>
			
	        
        

<?php }
	
	else { ?>
	
	 <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Shop</h1>
                        <ul class="crumb">
                            <li><a href="/">Home</a></li>
                            <li class="sep">/</li>
                            <li>Shop</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->
        
        <!-- content begin -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="products">
	                            
	                            <?php
			
									// Get products 
								    // --------------------------------
							          
										    $response = json_decode(file_get_contents($baseUrl.'products.json')); 
											   foreach ($response->products as $product) {
												   $result = json_decode(json_encode($product),true);
												   error_log( print_R(count($result),TRUE) );								
										
								?>
	                                <li class="col-md-4 product">
	                                    <a href="?product=<?php echo $result['handle']; ?>"><img src="<?php echo $result['image']['src'];?>" class="img-responsive product-image" alt=""></a>
	                                    <h4 class="product-title"><?php echo $result['title']; ?></h4>
	                                    <div class="price">$<?php echo $result['variants'][0]['price']; ?></div>
	                                    <a href="?product=<?php echo $result['handle']; ?>" class="btn btn-line">View</a>
	                                </li>
                                
                                <?php 
									}
									
								 ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php } ?>
        
        <!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <img src="../images/vossen-logo.png" class="logo-small" alt=""><br>
							Vossen has a passion for modern interior design and our mission is to provide the best quality artwork at the best price.                    
						</div>
                    <div class="col-md-4">
                        <div class="widget widget_recent_post">
                            <h3>Art Styles</h3>
                            <ul>
                                <li><a href="/shop/modern">Modern</a></li>
                                <li><a href="/shop/asian">Asian</a></li>
                                <li><a href="/shop/abstract">Abstract</a></li>
								<li><a href="/shop/wildlife">Wildlife</a></li>
                                <li><a href="/shop/minimalism">Minimalism</a></li>
                                <li><a href="/shop/city">City</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
	                    <div class="widget widget_recent_post">
						<h3>Sitemap</h3>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/shop">Shop</a></li>
                                <li><a href="/about">About Us</a></li>
                                <li><a href="/contact">Contact</a></li>                            
                                <li><a href="/privacy-policy">Privacy Policy</a></li>
                                <li><a href="/return-policy">Return Policy</a></li>
                                <li><a href="/shipping-and-delivery">Shipping and Delivery</a></li>                            
                            </ul>
		                </div>
		            </div>
		        </div>
            </div>
        </div>

            <div class="subfooter">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            &copy; Copyright 2016 - Vossen
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="social-icons">
                                <a href="https://www.facebook.com/Vossen-1805219913085693/" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
								<a href="https://www.instagram.com/vossenart/?hl=en" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        <!-- footer close -->
    </div>




    <!-- Javascript Files
    ================================================== -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jpreLoader.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/easing.js"></script>
    <script type="text/javascript" src="../js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="../js/plugins2.js"></script>
    <script src="../js/jquery.scrollto.js"></script>
    <script src="../js/owl.carousel.js"></script>
    <script src="../js/jquery.countTo.js"></script>
    <script src="../js/classie.js"></script>
    <script src="../js/video.resize.js"></script>
    <script src="../js/validation.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/jquery.magnific-popup.min.js"></script>
    <script src="../js/enquire.min.js"></script>
    <script src="../js/designesia.js"></script>
    <script type="text/javascript" src="../js/functions.js"></script>


    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
</body>
</html>
