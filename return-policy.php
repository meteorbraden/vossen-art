<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vossen - Modern Interior Artwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive Minimal Bootstrap Theme">
    <meta name="keywords" content="onepage,responsive,minimal,bootstrap,theme">
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/plugin.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    
    <!-- custom background -->
    <link rel="stylesheet" href="css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="css/color.css" type="text/css" id="colors">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="css/rev-settings.css" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="fonts/elegant_font/HTML_CSS/lte-ie7.js" type="text/css">
    <?php include_once("includes/analyticstracking.php") ?>
    <script src="js/shopify-cart.js"></script>
</head>
<body class="page-about">

    <div id="wrapper">
        <?php include 'includes/nav.php'; ?>

        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Return Policy</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">
            <section id="section-about-us-2" class="side-bg no-padding">
                <div class="container">
                    <div class="row">
                        <div class="inner-padding">
                            <div class="col-md-12 wow fadeInRight" data-wow-delay=".5s">
        					
							<p>Our policy lasts 14 days. If 14 days have gone by since your purchase, unfortunately, we can’t offer you a full refund or exchange.</p>
							
							<p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p>
							
							<p>Gift cards are non-returnable.</p>
							
							<p>To complete your return, we require a receipt or proof of purchase.</p>
							
							<p>Please do not send your purchase back to the manufacturer.</p>
							
							There are certain situations where only partial refunds are granted (if applicable):
							<ul>
								<li>Any item not in its original condition is damaged or missing parts for reasons not due to our error.</li>
								<li>Any item that is returned more than 14 days after delivery</li>
							</ul>
							
							<br />
							
							<h3>REFUNDS (IF APPLICABLE)</h3>
							
							<p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p>
							
							<p>If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>
							
							<h3>LATE OR MISSING REFUNDS (IF APPLICABLE)</h3>
							
							<p>If you haven’t received a refund yet, first check your bank account again.
							Then contact your credit card company, it may take some time before your refund is officially posted.</p>
							
							<p>Next contact your bank. There is often some processing time before a refund is posted.
							If you’ve done all of this and you still have not received your refund yet, please contact us at theblankfactor@gmail.com</p>
							
							<h3>SALE ITEMS (IF APPLICABLE)</h3>
							
							<p>Only regular priced items may be refunded, unfortunately, sale items cannot be refunded.</p>
							
							<h3>EXCHANGES (IF APPLICABLE)</h3>
							
							<p>We only replace items if they are defective or damaged. If you need to exchange it for the same item, send us an email at theblankfactor@gmail.com for additional information.</p>
							
							<h3>GIFTS</h3>
							
							<p>If the item was marked as a gift when purchased and shipped directly to you, you’ll receive a gift credit for the value of your return. Once the returned item is received, a gift certificate will be mailed or emailed to you.</p>
							
							<p>If the item wasn’t marked as a gift when purchased, or the gift giver had the order shipped to themselves to give to you later, we will send a refund to the gift giver and he will find out about your return.</p>
							
							<h3>SHIPPING</h3>
							
							<p>You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.</p>
							
							<p>Depending on where you live, the time it may take for your exchanged product to reach you, may vary.</p>
							
							<p>If you are shipping an item over $75, you should consider using a trackable shipping service or purchasing shipping insurance. We don’t guarantee that we will receive your returned item.</p>
							
							        					
        					
        					</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        		<?php include 'includes/footer.php'; ?>
    </div>


    <!-- Javascript Files
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jpreLoader.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.scrollto.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/video.resize.js"></script>
    <script src="js/validation.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/enquire.min.js"></script>
    <script src="js/designesia.js"></script>

    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

</body>
</html>
