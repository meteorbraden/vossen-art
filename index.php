<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vossen - Modern Interior Artwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Vossen Art - Modern, stylish, affordable art, guaranteed to make your home look and feel brand new.">
    <meta name="keywords" content="modern art, affordable art, abstract art, animal art, modern canvas prints, budget art, modern, interior design">
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/plugin.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="fonts/et-line-font/style.css" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="css/rev-settings.css" type="text/css">
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<?php include_once("includes/analyticstracking.php") ?>
    <script src="js/shopify-cart.js"></script>
</head>

<body id="homepage">

    <div id="wrapper">

        <?php include 'includes/nav.php'; ?>





        <!-- content begin -->
        <div id="content" class="no-bottom no-top">

            <!-- revolution slider begin -->
            <section id="section-welcome-5" class="full-height text-light" data-speed="3" data-type="background">


                <div class="center-y text-center hero-text">
                    <div class="spacer-double"></div>
                    <div class="text-slider border-deco">
                        <div class="text-item">
                            <h1 class="title-2">Modern Art</h1>
                            </span></div>
                        <div class="text-item">
                            <h1 class="title-2">Abstract Art</h1>
                            </span></div>    
                        <div class="text-item">
                            <h1 class="title-2">Minimalism Art</h1>
                            </span></div>
                        <div class="text-item">
                            <h1 class="title-2">Wildlife Art</h1>
                            </span></div>
                        <div class="text-item">
                            <h1 class="title-2">Asian Art</h1>
                            </span></div>    
                        <div class="text-item">
                            <h1 class="title-2">City Art</h1>
                            </span></div>
                    </div>
                    <div class="spacer-double"></div>
                    <div class="spacer-double"></div>
                </div>
            </section>
            <!-- revolution slider close -->

            <section id="section-side-1" class="side-bg margin-top">
                <div class="image-container col-md-5 pull-left" data-delay="0">
                    <div class="background-image"></div>
                </div>

				<!-- section begin -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6 wow fadeInRight" data-wow-delay=".5s">
                            <h2>Were all about looks</h2>
                            <div class="spacer-single"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h3>About Us</h3>
                                    <p>We understand that you have high standards when it comes to decorating your home, so we proudly offer the best modern art that we pick out exclusively for our valued clientele. After all, we understand that you're coming here because you are looking for quality designs that will keep your home fresh and modern while still holding onto the values that you are looking for in terms of the characteristics and the unique spirit that you are expecting from our products. We'll always work hard to make sure that you are getting the look and feel that you're interested in.</p>
                                </div>
                            </div>


                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>


			
			<!-- section close -->
			
			
            
            <!-- section begin -->
            <section id="section-about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                            <h1>Art Styles</h1>
                            <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                            <div class="spacer-single"></div>
                        </div>


                        <div class="col-md-4 wow fadeInLeft">
                            <a href="/shop/modern"><img src="images/artwork/modern/Modern-Nordic-Abstract-Landscape-Mountain-Sea-Vintage-Retro-Print-Poster-Wall-Art-Picture-Living-Room-Canvas_2.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">Modern Art</div></a>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".2s">
                            <a href="/shop/asian"><img src="images/artwork/asian/EHOME-Oil-Painting-Buddha-And-Lotus-Decoration-Painting-Home-Decor-On-Canvas-Modern-Wall-Art-Canvas_4.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">Asian Art</div></a>
                        </div>

                        <div class="col-md-4 wow fadeInRight">
                            <a href="/shop/abstract"><img src="images/artwork/abstract/Master-Artist-Handmade-High-Quality-Modern-Abstract-Oil-Painting-On-Canvas-Green-Canvas-Painting-For-Living_1.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">Abstract Art</div></a>
                        </div>

                    </div>
                    <div class="row m-top-30">
	                    <div class="col-md-4 wow fadeInLeft">
                            <a href="/shop/wildlife"><img src="images/artwork/wildlife/Room-Canvas-Print-Modern-Painting-Large-Canvas-1Pcs-No-Frame-Elephant-Painting-Canvas-Wall-Art-Picture_1.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">Wildlife Art</div></a>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".2s">
                            <a href="/shop/minimalism"><img src="images/artwork/minimalism/Heart-Shape-Canvas-Art-Print-Wall-Pictures-for-Home-Decoration-Wall-Art-Decor-FA153_6.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">Minimalism Art</div></a>
                        </div>

                        <div class="col-md-4 wow fadeInRight">
                            <a href="/shop/city"><img src="images/artwork/city/Turkey-Istanbul-city-landscape-canvas-printings-oil-painting-printed-on-canvas-home-wall-art-decoration-pictures_2.jpg" style="height: 360px; width: 360px;" class="img-responsive box-shadow" alt="">
							<div class="art-category">City Art</div></a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->

            </div>
		<?php include 'includes/footer.php'; ?>
    </div>



    <!-- Javascript Files
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jpreLoader.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.scrollto.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/video.resize.js"></script>
    <script src="js/validation.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/enquire.min.js"></script>
    <script src="js/designesia.js"></script>

    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



</body>
</html>
